/**
 * 
 */
package postgreSQLDatabase.newsfeed;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import forum.Post;
import settings.database.PostgreSQLConnection;

/**
 * @author Shubhi
 *
 */
public class Query {

	public static long addNewsfeedPost(String text,Long author_id,int privacy) {
		long post_id = 0;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addNewsfeedPost\"(?,?,?);");
			proc.setString(1, text);
			proc.setLong(2, author_id);
			proc.setInt(3, privacy);
			System.out.println(proc);
			ResultSet rs=proc.executeQuery();
			rs.next();
			post_id=rs.getLong(1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return post_id;
	}
	
	public static void addNewsfeedComment(String text,Long author_id,Long post_id) {
	
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addNewsfeedComment\"(?,?,?);");
			proc.setString(1, text);
			proc.setLong(2, author_id);
			proc.setLong(3, post_id);
			System.out.println(proc);
			proc.executeQuery();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void sendNewsfeedPost(Long post_id,Long users_id[]) {
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"sendNewsfeedPost\"(?,?);");
			proc.setLong(1, post_id);
			proc.setArray(2, PostgreSQLConnection.getConnection().createArrayOf("bigint",users_id));
			System.out.println(proc);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static NewsfeedPost getNewsfeedPost(Long post_id){
		NewsfeedPost post=new NewsfeedPost();
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getNewsfeedPost\"(?);");
			proc.setLong(1, post_id);
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				post.setPost_author(rs.getLong("post_author"));
				post.setComments_count(rs.getInt("post_comments"));
				post.setLikes_count(rs.getInt("post_likes"));
				post.setPost_id(rs.getLong("post_id"));
				post.setPost_privacy(rs.getInt("post_privacy"));
				post.setPost_text(rs.getString("post_text"));
				post.setPost_time(utilities.StringFormatter.convert(rs.getDate("post_time")));
				//System.out.println((rs.getLong("post_id")));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return post;
	}
	
	public static ArrayList<NewsfeedComments> getNewsfeedPostComments(Long post_id){
		ArrayList<NewsfeedComments> all_comment=new ArrayList<NewsfeedComments>();
		try {
			PreparedStatement proc = PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getNewsfeedPostComments\"(?);");
			proc.setLong(1, post_id);
			ResultSet rs = proc.executeQuery();
			System.out.println(proc);
			while(rs.next()){
				NewsfeedComments comment=new NewsfeedComments();
				comment.setComment_author(rs.getLong("comment_author"));
				comment.setComment_id(rs.getLong("comment_id"));
				comment.setComment_text(rs.getString("comment_text"));
				comment.setComments_count(rs.getInt("comment_count"));
				comment.setComment_time(utilities.StringFormatter.convert(rs.getDate("comment_time")));
				System.out.println((rs.getString("comment_count")));
				all_comment.add(comment);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return all_comment;
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(addNewsfeedPost("text",1000000107l, 1));
	   //addNewsfeedComment("wassup",1000000106l,5l);
		//Long users_id[]={1000000106l,1000000107l};
		//sendNewsfeedPost(5l,users_id );
		//getNewsfeedPost(1l);
		//System.out.println(getNewsfeedPostComments(1l));

	}

	
	public static void sendNewsfeedPostByGroup(Long post_id,String type) {
		try { 
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"sendNewsfeedPostByGroup\"(?,?);");
			proc.setLong(1, post_id);
			proc.setString(2,type);
			System.out.println(proc);
			proc.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
