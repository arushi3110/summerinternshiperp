/**
 * 
 */
package postgreSQLDatabase.templates;

/**
 * @author manisha pc
 *
 */
public class Tags {
	private String tagname;
	private String users;
	private String attributes;
	private long id;

	/**
	 * @return the tagname
	 */
	public String getTagname() {
		return tagname;
	}

	/**
	 * @param tagname
	 *            the tagname to set
	 */
	public void setTagname(String tagname) {
		this.tagname = tagname;
	}

	/**
	 * @return the users
	 */
	public String getUsers() {
		return users;
	}

	/**
	 * @param users
	 *            the users to set
	 */
	public void setUsers(String users) {
		this.users = users;
	}

	/**
	 * @return the attributes
	 */
	public String getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes
	 *            the attributes to set
	 */
	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
}
