/**
 * 
 */
package postgreSQLDatabase.authentication;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;

/**
 * @author Joey
 *
 */
public class Query {

	public static ArrayList<AutoSuggestUsers> getAutoSuggest(String input,String domain) {
		// System.out.println(input);
		ArrayList<AutoSuggestUsers> users_details = new ArrayList<AutoSuggestUsers>();
		PreparedStatement proc=null;
		JSONArray jArray = null;
		try {
		
			switch(domain){
			case "students":
				proc= settings.database.PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT * from public.\"getStudentIdAutoSuggest\"(?);");
				break;
			default:
				proc= settings.database.PostgreSQLConnection.getConnection()
				.prepareStatement("SELECT * from public.\"getUserAutoSuggest\"(?);");
				break;

			}
			proc.setString(1, input);
			System.out.println(proc);
			ResultSet rs = proc.executeQuery();
			while (rs.next()) {
				AutoSuggestUsers current = new AutoSuggestUsers();
				current.setId(rs.getLong("id"));
				current.setName(rs.getString("name"));
				if(!domain.equals("students"))current.setUser_type(rs.getString("user_type"));
				if(domain.equals("students")) current.setStudent_id(rs.getString("student_id"));
				current.setUsername(rs.getString("username"));
				current.setEmail(rs.getString("email"));
				// System.out.println(rs.getLong(0)+rs.getString(1)+rs.getString(2)+rs.getString(3));
				users_details.add(current);
			}
			// jArray = new JSONArray(rs.getString(1));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users_details;

	}

	public static String getUserUsername(Long erp_id) {
		PreparedStatement proc;
		try {
			proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"getUserUsernameById\"(?);");

			proc.setLong(1, erp_id);
			ResultSet rs = proc.executeQuery();
			rs.next();
			return rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static Long getUserId(String username) {
		PreparedStatement proc;
		try {
			proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"getUserId\"(?);");

			proc.setString(1, username);
			ResultSet rs = proc.executeQuery();
			rs.next();
			System.out.println(rs.getLong(1));
			return rs.getLong(1);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return (long) 0;

	}

	public static String getUserEmail(String username) {
		PreparedStatement proc;
		try {
			proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"getUserEmail\"(?);");

			proc.setString(1, username);
			ResultSet rs = proc.executeQuery();
			rs.next();
			System.out.println(rs.getString(1));
			return rs.getString(1);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return null;

	}

	public static Long VerifyForgotPassword(String username, String vercode) {
		PreparedStatement proc;
		try {
			proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"verifyForgotPassword\"(?,?);");

			proc.setString(1, username);
			proc.setString(2, vercode);
			ResultSet rs = proc.executeQuery();
			rs.next();
			return rs.getLong(1);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return 0l;

	}

	public static void updateVercode(Long user_id, String vercode) {
		PreparedStatement proc;
		try {
			proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"updateVercode\"(?,?);");

			proc.setLong(1, user_id);
			proc.setString(2, vercode);
			ResultSet rs = proc.executeQuery();
			rs.next();
			System.out.println(rs.getLong(1));

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	public static String getUserName(Long author_id) {
		String username = null;
		try {
			PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection()
					.prepareStatement("SELECT public.\"getUserName\"(?);");
			proc.setLong(1, author_id);
			proc.executeQuery();
			ResultSet rs = proc.executeQuery();
			while (rs.next()) {
				username = rs.getString("getUserName");

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return username;
	}

	public static void main(String[] args) {
		// getAutoSuggest("jo");
		// Long id=getUserId("meghagupta");
		// updateVercode(1000000101l,"vercodeupdated");
		String author_id = getUserName(1000000082L);
		//System.out.println(author_id);
	}

}
