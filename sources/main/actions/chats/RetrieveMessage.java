package actions.chats;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import postgreSQLDatabase.chats.Message;
import settings.database.PostgreSQLConnection;

/**
 * Servlet implementation class RetrieveMessage
 */
@WebServlet(name="RetrieveMessage",urlPatterns={"/RetrieveMessage"})
public class RetrieveMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RetrieveMessage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(500);
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-enerated method stub
		Long convo_id=Long.parseLong(request.getParameter("convo_id").toString());
		PrintWriter writer=response.getWriter();
		ArrayList<Message> messages=postgreSQLDatabase.chats.Query.getUnreadMessages(convo_id);
		

			Iterator<Message> iterator = messages.iterator();
			JSONArray message_array=new JSONArray();
			JSONObject message_object;
			while(iterator.hasNext()){
				message_object = new JSONObject();
				Message current=iterator.next();
				message_object.put("id",current.getId());
				message_object.put("author",current.getAuthor());
				message_object.put("username",current.getUsername());
				message_object.put("text",current.getText());
				message_object.put("timecomp",current.getTime_stamp().getTime());
				message_object.put("timestamp",new SimpleDateFormat("hh:mm a EEE dd MMM").format(current.getTime_stamp()));
				System.out.println(current.getId()+" "+current.getUsername()+" "+current.getText()+""+current.getTime_stamp());
				//writer.write(current.getId()+" "+current.getUsername()+" "+current.getText());
				message_array.put(message_object);
			}


			writer.write(message_array.toString());


		
	}
}
