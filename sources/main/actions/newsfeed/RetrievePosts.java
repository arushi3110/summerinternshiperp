package actions.newsfeed;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import actions.authentication.Session;
import postgreSQLDatabase.newsfeed.NewsfeedComments;
import postgreSQLDatabase.newsfeed.NewsfeedPost;

/**
 * Servlet implementation class RetrievePosts
 */
@WebServlet("/RetrievePosts")
public class RetrievePosts extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RetrievePosts() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter writer=response.getWriter();
		if(request.getParameter("action").equals("retrievePost")){
		long post_id=1l;
		 NewsfeedPost post = postgreSQLDatabase.newsfeed.Query.getNewsfeedPost(post_id);
		 JSONArray j_array=new JSONArray();
	     SimpleDateFormat df=new SimpleDateFormat("dd MMM yy hh:mm");
		 ArrayList<NewsfeedComments> all_comments = postgreSQLDatabase.newsfeed.Query.getNewsfeedPostComments(post_id);
		Iterator<NewsfeedComments> comment_iterator = all_comments.iterator();
		 while(comment_iterator.hasNext()){
			 JSONObject j_obj=new JSONObject();
			 NewsfeedComments current_comment = comment_iterator.next();
			 String comment_author=postgreSQLDatabase.authentication.Query.getUserName(current_comment.getComment_author());
			 j_obj.put("comment_author", comment_author);
			 j_obj.put("comment_id", current_comment.getComment_id());
			 j_obj.put("comments_count", current_comment.getComments_count());
			 j_obj.put("comment_likes", current_comment.getComment_likes());
			 j_obj.put("comment_text", current_comment.getComment_text());
			 j_obj.put("comment_time", df.format(current_comment.getComment_time()));
			 j_array.put(j_obj);
		 }
		 JSONObject current_post=new JSONObject();
		current_post.put("post_id", post.getPost_id());
		current_post.put("post_text", post.getPost_text());
		String post_author=postgreSQLDatabase.authentication.Query.getUserName(post.getPost_author());
		current_post.put("post_author",post_author);
		current_post.put("post_privacy", post.getPost_privacy());
		current_post.put("post_time", df.format(post.getPost_time()));
		current_post.put("post_comments", j_array.toString());
		current_post.put("likes_count", post.getLikes_count());
		current_post.put("comments_count", post.getComments_count());
		writer.write(current_post.toString());
	}
		
		if(request.getParameter("action").equals("addPost")){
			//long author=Long.parseLong(request.getParameter("author"));
			long author=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			String text=request.getParameter("text").toString();
			System.out.println(request.getParameter("privacy"));
			int privacy=Integer.parseInt(request.getParameter("privacy"));
			long post_id=postgreSQLDatabase.newsfeed.Query.addNewsfeedPost(text,author,privacy);
			if(privacy==0){
				postgreSQLDatabase.newsfeed.Query.sendNewsfeedPostByGroup(post_id,request.getSession().getAttribute("usertype").toString());
			}
		}
		
		
		
		if(request.getParameter("action").equals("addComment")){
			long author=Long.parseLong(request.getSession().getAttribute("erpId").toString());
			String text=request.getParameter("text").toString();
			long post_id=Integer.parseInt(request.getParameter("post_id"));
			postgreSQLDatabase.newsfeed.Query.addNewsfeedComment(text, author, post_id);
		}
		
		
	}
	

}
