package actions.forums;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import postgreSQLDatabase.forum.Query;
import postgreSQLDatabase.notifications.Notifications;
import actions.authentication.Session;

/**
 * Servlet implementation class AddNewComment
 */
@WebServlet("/AddNewComment")
public class AddNewComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddNewComment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//int count=0;
		try {
			System.out.println("hellllooooo");
		String data=request.getParameter("text");
		Long post_id=Long.parseLong(request.getParameter("post_id"));
		String post_name=postgreSQLDatabase.forum.Query.getPostName(post_id);
		HttpSession session = request.getSession();
		//System.out.println(data);
		Long author_id=Long.parseLong(String.valueOf(session.getAttribute("erpId")));
		String author_name=postgreSQLDatabase.authentication.Query.getUserUsername(author_id);
		//System.out.println(author_id);
		Long thread_id=postgreSQLDatabase.forum.Query.getThreadId(post_id);
		postgreSQLDatabase.forum.Query.addComment(data,post_id,author_id);
        
		Notifications notif=new Notifications();
		notif.setAuthor(author_name);
		notif.setLink("allPosts.jsp?thread_id="+thread_id);
		notif.setTimestamp(utilities.StringFormatter.convert(new java.util.Date()));
		
		//notif.setTimestamp(new Date(0));
		notif.setType("Forum");
		notif.setMessage("New comment in post "+post_name);
		notif.setExpiry(utilities.StringFormatter.convert(new java.util.Date()));
		ArrayList<Long> subscribers=postgreSQLDatabase.forum.Query.getAllSubscribersOfPost(post_id);
		subscribers.remove(author_id);
		postgreSQLDatabase.notifications.Query.addGroupNotification(notif, subscribers);
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
