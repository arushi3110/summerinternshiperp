<%@page import="utilities.StringFormatter"%>
<%@page import="java.util.Iterator"%>
<%@page import="utilities.NumberToWords"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="utilities.ServletUtils"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<%@page import="postgreSQLDatabase.feePayment.Payment"%>
<%@page import="postgreSQLDatabase.feePayment.Transaction"%>
<%@page import="org.json.JSONObject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html xmlns="http://www.w3.org/1999/xhtml"> 
<head>
<title>IIITK Challan</title>
<style>
@page { size: A4 landscape;}
</style>
</head>
<body>
    <%
    
    long ref_no=Long.parseLong(request.getParameter("ref_no"));
	
	//long ref_no=17L;
    Payment payment=postgreSQLDatabase.feePayment.Query.getFeePaymentByRefNo(ref_no);
    Transaction transaction=postgreSQLDatabase.feePayment.Query.getFeeTransactionById(payment.getTransaction_id());
    Student student=Query.getRegistrationStudentData(transaction.getReg_id()); 
    Date date=new Date();
							SimpleDateFormat format=new SimpleDateFormat("MM/dd/yyyy");
							String current_date=format.format(date);
							%>
		
			<table border="1">
			<tbody>
			<tr>
				<td>
				<div class="col-md-4">
				
					<div class="row" style="font-size:80%;">
					<table><tr><td><img  alt="img" height="65px" width="65px"  src="<%=ServletUtils.getBaseUrl(request)%>/image/sbi.png" class="image pull-right"/>
						</td><td><p class="text-center" align="center"><b>Indian Institute Of Information Technology Kota(Rajasthan)</b><br/>(Bank Copy)</p></td>
						<td><img  alt="img"  height="65px" width="65px" src="<%=ServletUtils.getBaseUrl(request)%>/image/iiitkota-logo.png"/></td>
						</tr></table>
						
					</div>
					
					<div class="row invoice-info" style="font-size:80%;align-items:center;">
						<div class="invoice-col" align="center" >
							<b>Category:</b> <%=student.getCategory() %>
							<b>Gender(M/F):</b> <%=student.getGender() %>
							
							
								
                            
						</div>
						
						<div class="invoice-col" align="center">
							<b>Student ID:</b> <%=student.getStudent_id() %><br/>
							
							
							<b>Date:</b> <%=current_date %>
							<p align="center" style="text-decoration: underline;"><b>Institue Fee Detail for  <%=current_date.substring(6) %>  <%=utilities.StringFormatter.getOddEven(transaction.getSemester()) %> Semester</b></p>
						</div>
					</div>
					
					<div class="row">
						<div class="table-responsive">
							<table class="table table-striped" style="font-size:80%" >
								<tbody >
									<tr>
										<td><b>Account To Credit</b></td>
										<td> 32896056885</td>
									</tr>
									<tr>
										<td><b>Institute Code</b></td>
										<td>CSE</td>
									</tr>
									<tr>
										<td><b>Student Name</b></td>
										<td><%=student.getName() %></td>
									</tr>
									<tr>
										<td><b>Roll No.</b></td>
										<td><%=student.getStudent_id() %></td>
									</tr>
									<tr>
										<td><b>Class/Program</b></td>
										<td><%=student.getProgram_allocated() %></td>
									</tr>
									<tr>
										<td><b>Section/Semester</b></td>
										<td><%=transaction.getSemester() %></td>
									</tr>
									<tr>
										<td><b>Amount</b></td>
										<td><%=payment.getAmount() %></td>
									</tr>
									<tr>
										<td><b>Amount In Words</b></td>
										<td><%=NumberToWords.convert(payment.getAmount())%></td>
									</tr>
										<% 
								  JSONObject details1= payment.getDetails();
                		 Iterator<String> iterator1= details1.keys();
                		  while(iterator1.hasNext()){
                			  String key=iterator1.next();
                			  if(key.equals("amount"))
                				  continue;
                			 out.print("<tr style='border:none !important;'><td style='border:none !important'><strong>"+StringFormatter.TitleCase(key ).replaceAll("_", " ")+":</strong></td><td style='border:none !important'>"+details1.get(key)+"</td></tr>");
                		 }%>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="row" style="font-size:80%">
						<h4 class="text-center">(To be filled by bank)</h4>
						Branch SOL ID <span class="pull-right">____________________</span><br/>
						Branch Transaction ID <span class="pull-right">____________________</span><br/><br/><br/><br/>
						
						<table><tr><td>Seal &amp; Stamp of SBI Bank</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature of Depositor</td></tr>
						</table>
						<h6>If Incorrect fee is deposited, Student registration may be cancelled/terminated. Registration is Subject to fulfilling eligibility criteria</h6>
					</div>
				</div>
				
				</td>
				
				<td>
				<div class="col-md-4">
				
					<div class="row" style="font-size:80%;">
					<table><tr><td><img height="65px" width="65px"  alt="img"  src="<%=ServletUtils.getBaseUrl(request)%>/image/sbi.png" class="image pull-right"/>
						</td><td><p class="text-center" align="center"><b>Indian Institute Of Information Technology Kota(Rajasthan)</b><br/>(Institute Copy)</p></td>
						<td><img height="65px" width="65px"  alt="img"  src="http://localhost:8080/erp/image/iiitkota-logo.png"/></td>
						</tr></table>
						
					</div>
					
					<div class="row invoice-info" style="font-size:80%;align-items:center;">
						<div class="invoice-col" align="center" >
							<b>Category:</b> <%=student.getCategory() %>
							<b>Gender(M/F):</b> <%=student.getGender() %>
							
							
								
                            
						</div>
						
						<div class="invoice-col" align="center">
							<b>Student ID:</b> <%=student.getStudent_id() %><br/>
							
							
							<b>Date:</b> <%=current_date %>
							<p align="center" style="text-decoration: underline;"><b>Institue Fee Detail for  <%=current_date.substring(6) %>  <%=utilities.StringFormatter.getOddEven(transaction.getSemester()) %> Semester</b></p>
						</div>
					</div>
					
					<div class="row">
						<div class="table-responsive">
							<table class="table table-striped" style="font-size:80%" >
								<tbody >
									<tr>
										<td><b>Account To Credit</b></td>
										<td> 32896056885</td>
									</tr>
									<tr>
										<td><b>Institute Code</b></td>
										<td>CSE</td>
									</tr>
									<tr>
										<td><b>Student Name</b></td>
										<td><%=student.getName() %></td>
									</tr>
									<tr>
										<td><b>Roll No.</b></td>
										<td><%=student.getStudent_id() %></td>
									</tr>
									<tr>
										<td><b>Class/Program</b></td>
										<td><%=student.getProgram_allocated() %></td>
									</tr>
									<tr>
										<td><b>Section/Semester</b></td>
										<td><%=transaction.getSemester() %></td>
									</tr>
									<tr>
										<td><b>Amount</b></td>
										<td><%=payment.getAmount() %></td>
									</tr>
									<tr>
										<td><b>Amount In Words</b></td>
										<td><%=NumberToWords.convert(payment.getAmount())%></td>
									</tr>
										<% 
								  JSONObject details2= payment.getDetails();
                		 Iterator<String> iterator3= details2.keys();
                		  while(iterator3.hasNext()){
                			  String key=iterator3.next();
                			  if(key.equals("amount"))
                				  continue;
                			 out.print("<tr style='border:none !important;'><td style='border:none !important'><strong>"+StringFormatter.TitleCase(key ).replaceAll("_", " ")+":</strong></td><td style='border:none !important'>"+details2.get(key)+"</td></tr>");
                		 }%>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="row" style="font-size:80%">
						<h4 class="text-center">(To be filled by bank)</h4>
						Branch SOL ID <span class="pull-right">____________________</span><br/>
						Branch Transaction ID <span class="pull-right">____________________</span><br/><br/><br/><br/>
						
						<table><tr><td>Seal &amp; Stamp of SBI Bank</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature of Depositor</td></tr>
						</table>
						<h6>If Incorrect fee is deposited,Student registration may be cancelled/terminated. Registration is Subject to fulfilling eligibility criteria</h6>
					</div>
				</div>
				
				</td>
				<td>
				<div class="col-md-4">
				
					<div class="row" style="font-size:80%;">
					<table><tr><td><img height="65px" width="65px"  alt="img"  src="<%=ServletUtils.getBaseUrl(request)%>/image/sbi.png" class="image pull-right"/>
						</td><td><p class="text-center" align="center"><b>Indian Institute Of Information Technology Kota(Rajasthan)</b><br/>(Student Copy)</p></td>
						<td><img height="65px" width="65px"  alt="img"  src="http://localhost:8080/erp/image/iiitkota-logo.png"/></td>
						</tr></table>
						
					</div>
					
					<div class="row invoice-info" style="font-size:80%;align-items:center;">
						<div class="invoice-col" align="center" >
							<b>Category:</b> <%=student.getCategory() %>
							<b>Gender(M/F):</b> <%=student.getGender() %>
							
							
								
                            
						</div>
						
						<div class="invoice-col" align="center">
							<b>Student ID:</b> <%=student.getStudent_id() %><br/>
							
							
							<b>Date:</b> <%=current_date %>
							<p align="center" style="text-decoration: underline;"><b>Institue Fee Detail for  <%=current_date.substring(6) %>  <%=utilities.StringFormatter.getOddEven(transaction.getSemester()) %> Semester</b></p>
						</div>
					</div>
					
					<div class="row">
						<div class="table-responsive">
							<table class="table table-striped" style="font-size:80%" >
								<tbody >
									<tr>
										<td><b>Account To Credit</b></td>
										<td> 32896056885</td>
									</tr>
									<tr>
										<td><b>Institute Code</b></td>
										<td>CSE</td>
									</tr>
									<tr>
										<td><b>Student Name</b></td>
										<td><%=student.getName() %></td>
									</tr>
									<tr>
										<td><b>Roll No.</b></td>
										<td><%=student.getStudent_id() %></td>
									</tr>
									<tr>
										<td><b>Class/Program</b></td>
										<td><%=student.getProgram_allocated() %></td>
									</tr>
									<tr>
										<td><b>Section/Semester</b></td>
										<td><%=transaction.getSemester() %></td>
									</tr>
									<tr>
										<td><b>Amount</b></td>
										<td><%=payment.getAmount() %></td>
									</tr>
									<tr>
										<td><b>Amount In Words</b></td>
										<td><%=NumberToWords.convert(payment.getAmount())%></td>
									</tr>
										<% 
								  JSONObject details3= payment.getDetails();
                		 Iterator<String> iterator4= details3.keys();
                		  while(iterator4.hasNext()){
                			  String key=iterator4.next();
                			  if(key.equals("amount"))
                				  continue;
                			 out.print("<tr style='border:none !important;'><td style='border:none !important'><strong>"+StringFormatter.TitleCase(key ).replaceAll("_", " ")+":</strong></td><td style='border:none !important'>"+details3.get(key)+"</td></tr>");
                		 }%>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="row" style="font-size:80%">
						<h4 class="text-center">(To be filled by bank)</h4>
						Branch SOL ID <span class="pull-right">____________________</span><br/>
						Branch Transaction ID <span class="pull-right">____________________</span><br/><br/><br/><br/>
						
						<table><tr><td>Seal &amp; Stamp of SBI Bank</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature of Depositor</td></tr>
						</table>
						<h6>If Incorrect fee is deposited, student registration may be cancelled/terminated.Registration is Subject to fulfilling eligibility criteria</h6>
					</div>
				</div>
				
				</td>
				</tr></tbody></table>	
		
</body>
</html>
